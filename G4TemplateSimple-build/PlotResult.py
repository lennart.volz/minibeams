import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema,argrelmax,argrelmin

def main():
    dosediff = []
    for n,arg in enumerate(sys.argv[1:]):
        data = np.loadtxt(arg,delimiter=',',skiprows=3)
        x = np.array(data[:,0])
        y = np.array(data[:,1])
        z = np.array(data[:,2])
        dose = data[:,3]
        dose = np.array(dose).reshape((int(x.max())+1,int(y.max())+1,int(z.max()+1)))
        dosesum = np.zeros((dose.shape[0],dose.shape[2]))
        for i in range(dose.shape[0]): 
            for j in range(dose.shape[2]):
                dosesum[i,j] = dose[i,:,j].sum()
        dosediff.append(dosesum)

    cmap = 'YlGnBu_r'
    vmin = 0
    vmax = dosesum.max()
    if(len(sys.argv[1:])>1): 
        dosediff = np.array(dosediff).reshape(2,dose.shape[0],dose.shape[2])
        dosesum = 100*(np.array(dosediff[0,:,:]) - np.array(dosediff[1,:,:]))/np.array(dosediff[0,:,:])
        cmap = 'RdBu'
        vmin = -30
        vmax = 30
    else: dosediff=np.array(dosediff)
    plt.imshow(dosesum, extent = [0,z.max()*0.5,0.,x.max()*0.1],cmap=cmap,vmin=vmin, vmax=vmax)
    if(len(sys.argv[1:])>1): plt.colorbar(label="Difference [%]")
    else: plt.colorbar(label="Dose [Gy]")
    plt.xlabel("Depth [mmH2O]")
    plt.ylabel("Lateral position [mm]")
    plt.savefig("Difference-Orig-Chamfer1.png", format='png',bbox_inches='tight', transparent=False)
    plt.show()

    z = np.linspace(0.,z.max()*0.5, num=len(dosediff[0,0,:]))
    plt.plot(z,dosediff[0,:,:].sum(axis=0), label="Orig. Collimator")
    if(len(sys.argv[1:])>1): plt.plot(z,dosediff[1,50:-50,:].sum(axis=0), label="Second Collimator")
    plt.xlabel("Depth [mmH2O]")
    plt.ylabel("Integral dose [Gy]")
    plt.show()

    dosesum=np.array(dosesum)
    xx = np.linspace(0., x.max()*0.1, num=len(dosediff[0,:,0]))
    zz = np.linspace(0., z.max(), num=int(dose.shape[2]/10))
    PVDR = np.zeros(len(zz))
    for i in range(0,dose.shape[2]-200,10):
         print(i)
         maxima = argrelmax(dosediff[0,:,i],order=15)[0]
         minima = argrelmin(dosediff[0,:,i],order=15)[0]
         mean = []
         for maxi in maxima[2:-2]: 
             mean.append(dosediff[0,maxi,i]/dosediff[0,minima[np.where(minima>maxi)][0],i])
         mean = np.mean(mean)
         PVDR[int(i/10)] = mean

         plt.plot(xx, dosediff[0,:,i]/dosediff[0,:,i].max(), label="Orig. Collimator")
         #plt.plot(xx[maxima], dosediff[0,maxima,i]/dosediff[0,:,i].max(),'ro')
         #plt.plot(xx[minima], dosediff[0,minima,i]/dosediff[0,:,i].max(),'bo')
         if(len(sys.argv[1:])>1): plt.plot(xx,dosediff[1,:,i]/dosediff[1,:,i].max(), label="Chamfer Collimator")
         plt.legend(title=f'{"Depth "}{i*0.5}{"mm"}')
         plt.xlabel("Lateral position [mm]")
         plt.ylabel("Normalized dose")
         plt.savefig(f'{"LateralProfileat2cmOrig-Chamfer1_"}{i}{".png"}', format='png',bbox_inches='tight',transparent=False)
         plt.show()
    plt.plot(zz,PVDR)
    plt.xlabel("Depth [mm]")
    plt.ylabel("PVDR")
    plt.show()

if __name__ == "__main__":
    main()
