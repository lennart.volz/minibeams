#ifndef CONFIG_h
#define CONFIG_h

#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <string>
#include <map>
#include <fstream>

#include "G4String.hh"

using namespace std; 

class Config{
	public:
		Config();
		~Config(); 
		static inline Config* GetInstance(){return theConfig;}
		bool fHelp, fDebug, fVis;
		int fN; 
		double fEnergy;
		int fA,fZ; 	
		bool fGaus;
	        double fSpotSize, fSpotStep;	
		double fDivergence; 
		double fShiftX; 
		bool fPE;
		bool fModulator;
	        bool fCollimator; 	
	        double fInclination;
		double fAirGap; 
		double fSource;
		double fModCollGap; 
		G4String fModulatorMesh; 	
		G4String fPEMesh; 
		G4String fCollimatorMesh;
	        G4String fOut;
                bool fFromRST; 
                G4String fRST;  
                G4double fFieldSizeX, fFieldSizeY;
                bool fMITSetup;  	
                bool fLET;
                bool fUseSTL;  
		void ParseCommandLine(int, char**);
	private: 
		static Config *theConfig; 


};
#endif
