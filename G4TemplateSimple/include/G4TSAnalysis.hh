#ifndef G4TSAnalysis_hh
#define G4TSAnalysis_hh
#include "G4ThreeVector.hh"
#include "globals.hh"
#include "TH3D.h"
#include "TTree.h"
#include "Config.hh"

using namespace std;
class G4Step;
class TFile ;
class TH3D  ;
class G4TSDetectorConstruction;
class G4TSAnalysis
{
public:

  G4TSAnalysis(G4String);
  ~G4TSAnalysis();
  static inline G4TSAnalysis* GetInstance() { return theAnalysis; }
  void analyseHit(G4Step*,G4String);
  void analyseCMOSHits(G4Step*,G4int);
  void analyseFluence(G4Step*); 
  void saveCMOSHits();
 
 
  TFile *f1;
  void Save();

  G4double xmin,xmax,ymin,ymax,zmin,zmax; 
  TH3D *energy_hist; 
  TH3D *LET_hist; 
  TH3D* DMLET_hist; 
  TTree *t;
  TTree *flu; 
  G4double x[6], y[6],z[6], Edep[6],Ekin[6];
  G4int PID, EID; 
  G4double x_flu, y_flu,z_flu, Edep_flu,Ekin_flu;
  G4int PID_flu, EID_flu; 
private:

  static G4TSAnalysis* theAnalysis;
  G4TSDetectorConstruction   *theDetector ;
  Config *theConfig; 
};
#endif

