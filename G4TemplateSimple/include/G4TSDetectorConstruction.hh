//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#ifndef G4TSDetectorConstruction_h
#define G4TSDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4VSolid.hh"
#include "Config.hh"
#include "globals.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;

/// Detector construction class to define materials and geometry.

class G4TSDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    G4TSDetectorConstruction();
    static G4TSDetectorConstruction* GetInstance(){return theDetector;}; 
    virtual ~G4TSDetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    // World variables
    G4double world_sizeX, world_sizeY, world_sizeZ; 

    // Fluence scorer for scoring secondary particle fluence right after collimatro
    G4double fluscorer_thickness; 

    // Water tank variables
    G4double tank_posZ, tank_SizeX, tank_SizeY, tank_SizeZ;  


    //Nozzle variable (only used for MIT setup)
    G4double nozzle_thickness = 316;
 
    // Collimator
    G4VSolid *mesh_solid; 
    G4double mesh_Xmin,mesh_Xmax,mesh_Ymin,mesh_Ymax,mesh_Zmin,mesh_Zmax, mesh_SizeX, mesh_SizeY, mesh_SizeZ;
    G4double collimator_pos; 

    // Modulator
    G4VSolid *Modmesh_solid; 
    G4double Modmesh_Xmin,Modmesh_Xmax,Modmesh_Ymin,Modmesh_Ymax,Modmesh_Zmin,Modmesh_Zmax, Mod_SizeX, Mod_SizeY, Mod_SizeZ;
    G4double modulator_pos; 

    // Neutron shield or collimator
    G4VSolid *PEmesh_solid; 
    G4double PEmesh_Xmin,PEmesh_Xmax,PEmesh_Ymin,PEmesh_Ymax,PEmesh_Zmin,PEmesh_Zmax, PE_SizeX, PE_SizeY, PE_SizeZ;
    G4double PE_pos; 

    G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }
    G4String theName;
    Config* theConfig;
  protected:
    static G4TSDetectorConstruction* theDetector; 
    G4LogicalVolume*  fScoringVolume;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

