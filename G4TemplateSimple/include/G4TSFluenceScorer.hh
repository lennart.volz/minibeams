#ifndef G4TSFLUENCESCORER_HH_
#define G4TSFLUENCESCORER_HH_


#include "G4VSensitiveDetector.hh"


#include "G4Step.hh"
#include "G4Track.hh"
#include "G4HCofThisEvent.hh"
#include "G4TouchableHistory.hh"
#include "G4TSAnalysis.hh"
class G4TSAnalysis;
class G4TSFluenceScorer : public G4VSensitiveDetector
{

public:
  G4TSFluenceScorer(G4String);
  virtual ~G4TSFluenceScorer(){};
  G4String theName;
  G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
  void EndOfEvent(G4HCofThisEvent* ){};
  void clear(){};
  void DrawAll(){};
  void PrintAll(){};
private:
  G4TSAnalysis* theAnalysis;


};




#endif

