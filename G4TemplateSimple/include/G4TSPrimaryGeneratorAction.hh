//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#ifndef G4TSPrimaryGeneratorAction_h
#define G4TSPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"
#include "Config.hh"
#include "Randomize.hh"
#include "CLHEP/Random/RandGeneral.h"

class G4ParticleGun;
class G4Event;
class G4Box;

/// The primary generator action class with particle gun.
///
/// The default kinematic is a 6 MeV gamma, randomly distribued 
/// in front of the phantom across 80% of the (X,Y) phantom size.

class G4TSPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    G4TSPrimaryGeneratorAction();    
    virtual ~G4TSPrimaryGeneratorAction();

    // method from the base class
    virtual void GeneratePrimaries(G4Event*);         
  
    // method to access particle gun
    const G4ParticleGun* GetParticleGun() const { return fParticleGun; }

    G4double xmin,xmax,ymin,ymax,zmin,zmax;
    std::vector<double> x_center, y_center,foci,energies;
    std::vector<int> nParticles; 
    double *mPDF; 
    CLHEP::RandGeneral *mDistriGeneral; 
    int nSpotsX =0; 
    int nSpotsY = 0;  
    int nSpots = 0; 
    int spot_x =0; 
    int spot_y = 0; 
    G4double X, Y;
    G4double angle_theta, angle_phi; 

    int userA,userZ; 
    double div,div_x,div_y; 
    
    G4double ionE; 
    G4ParticleDefinition *particle;

    Config *theConfig; 
  private:
    G4ParticleGun*  fParticleGun; // pointer a to G4 gun class
    G4Box* fEnvelopeBox;
    std::vector<double> linspace(double, double, double);
    void ParseRST(G4String); 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
