#include "G4UserTrackingAction.hh"
#include "G4TSAnalysis.hh"

class G4TSTrackingAction : public G4UserTrackingAction
{
   public:

     // Constructor & Destructor
     G4TSTrackingAction();
     virtual ~G4TSTrackingAction(){}

     // Member functions
     virtual void PreUserTrackingAction(const G4Track*){} 
     virtual void PostUserTrackingAction(const G4Track*); 
    private:
     G4TSAnalysis *theAnalysis;  
};
