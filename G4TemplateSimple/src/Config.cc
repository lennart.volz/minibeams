#include "Config.hh"
#include "getopt.h"
#include <iostream>
#include "globals.hh"

Config* Config::theConfig = NULL;

Config::Config(){
  theConfig = this;
  fN = 10000; 
  fEnergy = 180; //[MeV/u] Beam energy
  fA = 12; 
  fZ = 6; 
  fCollimatorMesh = "none";
  fPEMesh = "none"; 
  fModulatorMesh = "none";
  fPE = false;  
  fCollimator = false;
  fModulator = false;
  fFieldSizeX = 0; 
  fFieldSizeY = 0;  
  fShiftX = 0;  
  fSpotSize = 0; // [mm] Beam focus
  fDivergence = 0; //[mrad] Beam divergence
  fSpotStep = 0;
  fGaus = false; // Use pencil beam scanned field or flat field
  fHelp = false; 
  fDebug = false;
  fVis = false;
  fInclination = 0;
  fSource = 0.;
  fAirGap = 0.;
  fModCollGap = 0;   
  fMITSetup = false; 
  fOut = ".";
  fFromRST = false;
  fLET = false;
  fUseSTL = true;   
  fRST = "";  
}

Config::~Config(){
theConfig=NULL;
}

void Config::ParseCommandLine(int argc, char *argv[]) {
   //
   // Parse the command line and determine which 
   // files are to be converted and which options
   // are requested.
   //

   int verbose(0), debug(0);
   
   while (1) {
      
      // Define the command line options
      const option long_options[] = {
         {"verbose", no_argument, &verbose, 'v'},//
         {"debug", no_argument, &debug, 'd'},//
         {"help", no_argument, 0, 'h'},//
	 {"npart", required_argument, 0, 'n'},//
         {"energy", required_argument, 0, 'e'}, //
	 {"Anumber", required_argument, 0, 'a'},//
	 {"Znumber", required_argument, 0, 'z'},//
	 {"particle", required_argument, 0, 'p'},//
	 {"gaus", no_argument, 0, 'g'},//
	 {"spotsize", required_argument, 0, 's'},//
	 {"spotdivergence", required_argument, 0, 'r'},//
	 {"spotspace", required_argument, 0, 'l'},//
	 {"shiftx", required_argument, 0, 'k'},//
	 {"thermneutron", required_argument, 0, 't'},//
	 {"collimator", required_argument, 0, 'c'},//
	 {"inclination", required_argument,0, 'i'},
	 {"output", required_argument, 0, 'o'},//
	 {"beamsource", required_argument, 0, 'b'},//
	 {"airgap", required_argument, 0, 'w'},//
	 {"modulator",required_argument, 0, 'm'},//
	 {"farfield",required_argument, 0, 'f'},//
         {"fieldy", required_argument, 0, 'y'},//
         {"fieldx", required_argument, 0, 'x'},//
         {"claire",no_argument,0,'q'},//
         {"rst", required_argument, 0, 'u'}, //
         {"let", no_argument,0,'j'},//
         {"nostl",no_argument,0,'0'},//
         {0, 0, 0, 0}
      };
   
      // getopt_long stores option index here
      int option_index = 0;
     
      // Get the options from command line
      const auto opt = getopt_long (argc, argv, "u:n:e:z:a:p:s:r:l:k:x:y:m:f:c:t:o:b:w:i:hvdgqj0",
                            long_options, &option_index);
     
      // Abort if end of options is reached
      if (opt == -1) {break;}
     
      switch (opt) {
        
      case 0:
         //
         // Do nothing if long option without
         // index occurs ( =flags, e.g. '--verbose' ).
         // This is already taken care for above.
         //
	 fHelp = true;
         break;
      case 'n': 
	 fN = atoi(optarg);
	 break; 
      case 'e':
         //
         // Prefix chosen for output files
         //
         fEnergy = atof(optarg);
         break;
      case 'a':
	fA = atoi(optarg);
        break;	
      case 'z': 
	fZ = atoi(optarg); 
	break;
      case 'p':
	if(strcmp(optarg, "carbon")==0 || strcmp(optarg, "Carbon")==0){ fA = 12; fZ = 6; }
	else if(strcmp(optarg, "helium")==0 || strcmp(optarg, "Helium")==0){ fA = 4; fZ =2;} 
	else if(strcmp(optarg, "argon")==0 || strcmp(optarg, "Argon")==0){ fA = 40; fZ = 18;}
	else if(strcmp(optarg, "proton") ==0 || strcmp(optarg, "Proton")==0){fA = 1; fZ = 1;}
	else {G4cout << "Particle not found! Abort." << G4endl; exit(0);}
	break; 
      case 'h':
         //
         // User requests help
         //
         fHelp = true;
         break;

      case 'v':
         //
         // Activate verbose
         //
         fVis = true;
         break;

      case 'd':
         //
         // Activate debug mode
         //
         fDebug = true;
         break;

      case 'g':
         //
         //
         fGaus = true;
         break;

      case 's':
         //
         // 
         //
	 fSpotSize = atof(optarg);
         break;

      case 'r':
	 fDivergence = atof(optarg);
         break;

      case 'l':
         fSpotStep = atof(optarg);
	 break;
      case 'b':
	 fSource = atof(optarg); 
	 break; 
      case 'w': 
	 fAirGap = atof(optarg); 
	 break;
      case 'f':
	 fModCollGap = atof(optarg); 
	 break; 
      case 'k':
	 fShiftX = atof(optarg);  
	 break;
      case 'i': 
	fInclination = atof(optarg); 
        break; 	
      case 't':
	 fPE = true; 
	 fPEMesh = optarg;
	 break;
      case 'm': 
	 fModulatorMesh = G4String(optarg); 
	 fModulator = true; 
	 break; 
      case 'c':
	 fCollimatorMesh = G4String(optarg); //Collimator name 
	 fCollimator = true; 
         break;
      case 'x': 
         fFieldSizeX = atof(optarg);
         break; 
      case 'y': 
         fFieldSizeY = atof(optarg); 
         break; 
      case 'q':
         fMITSetup = true; 
         break; 
      case 'u':
         fFromRST = true;  
         fRST = optarg;
         break;
      case 'j':
         fLET = true; 
         break;   
      case 'o':
	 fOut = optarg; 
	 break; 
      case '0': 
         fUseSTL=false; 
         break;
      case '?': 
	 G4cout << "Unrecognized option" << opt << G4endl;
	 fHelp = true;
	 break;
      
      default:
         //
         // Print help here
         //
         fHelp = true;
         break;
      }
     
    
   }
   if(fGaus && !(fSpotSize>0 && fSpotStep>0)){
     	G4cout << "A gaussian beam needs a specific standard deviation in lateral position and also in angular divergence. Abort.\n" << G4endl;
	exit(0);
   }

   if(fHelp){
	G4cout << 
	 "This is a simulation for minibeam collimator testing. Please use it like the following: \n"
  	 "Run the script: ./templateSimple <args>\n"
         "-h Help; you are here\n"
 	 "-d Debug. Usefule console output\n"
 	 "-v Verbose. Prints G4 internal stuff to console.\n"
	 "-n Number of particles in run(int). Default 10000"
 	 "-e Energy in MeV per u (float). Sets the beam energy\n"
	 "-a Atomic number of projectile (int). Default 12\n"
	 "-z Atomic charge of projectile (int). Default 6\n"
	 "-p Particle(string). Either Carbon, Helium, Proton or Argon.\n"
 	 "-g Gaussian beam (boolean). If set, a gauss instead of flat homogeneous field is used.\n"
 	 "-s Spot size in mm (float). If Gaus: Spot sigma in lateral position (spot is assumed symmetric in x and y)\n."
 	 "-l Spot spacing mm (float). If Gaus: Lateral spacing between adjacent spots.\n" 
	 "-k Shift in x(float). Shifts the pencil beam scanning field in horizontal direction to be misaligned with the center of the mesh by +x. Default 0. \n"
 	 "-r Beam divergence in mrad (float). \n" 
	 "-i Beam inclination with respect to the z axis in mrad (float).\n"
	 "-b Beam source point distance from isocenter in mm (float).\n"
	 "-c Mesh (file). Stl mesh representing the collimator\n"
	 "-t Neutron shield (file). Stl mesh representing the PE neutron shield.\n"
	 "-m Modulator mesh (file). Stl Mesh representing a range mdoulator.\n"
	 "-f Distance between modulator and collimator in mm (float).\n"
         "-x Field size in x\n"
         "-y Field size in y\n"
         "-u Red the field parameters from an input RST file. Give a path to an rst file (string).\n"
         "-j LET scorer. Boolean flag. If set, the let will be scored in a 3D root histogram. Slows down simulation drastically.\n"
         "-w Air gap between collimator and water tank in mm. Give a float. Default. 0mm.\n"
         "-q Build the MIT Measurement setup instead of the general setup with water tank. Boolean flag. Default: false.\n"
	 "-o Output dir (string). Output directory."; 
	exit(0);
     } else {
	 printf("The following settings were used: \n -d %d\n -v %d\n -n %d\n -e %f\n-a %d\n-z %d\n-g %d\n-s %f\n-l %f\n-r %f\n-k %f\n-w %f\n-b %f\n-i %f\n-f %f\n-x %f\n-y %f\n-u %d\n-j %d\n-q %d\n-c %s\n-m %s\n-t %s\n-o %s\n", 
			 fDebug, fVis, fN, fEnergy, fA, fZ, fGaus, fSpotSize, fSpotStep, fDivergence, fShiftX, fAirGap, fSource, fInclination, fModCollGap, fFieldSizeX, fFieldSizeY, fRST, fLET, fMITSetup, 
			 fCollimatorMesh.c_str(), fModulatorMesh.c_str(), fPEMesh.c_str(), fOut.c_str());


     }
    
     
}
