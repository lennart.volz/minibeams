#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4TSAnalysis.hh"
#include "Randomize.hh"
#include "G4TSDetectorConstruction.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4Track.hh"
#include "TFile.h"
#include "TH3D.h"
#include "TDirectory.h"
#include "Config.hh"
#include "TTree.h"
#include "G4RunManager.hh"

G4TSAnalysis* G4TSAnalysis::theAnalysis=NULL;
G4TSAnalysis::~G4TSAnalysis(){theAnalysis=NULL;}

G4TSAnalysis::G4TSAnalysis(G4String theName){
  theAnalysis  = this;

  //theGenerator = PrimaryGeneratorAction::GetInstance();
  theDetector  = G4TSDetectorConstruction::GetInstance();
  theConfig = Config::GetInstance(); 
  f1 = new TFile(theName.data(),"recreate");

  xmin = -theDetector->tank_SizeX; 
  xmax = theDetector->tank_SizeX; 
  ymin = -theDetector->tank_SizeY;
  ymax = theDetector->tank_SizeY; 
  zmin = theDetector->tank_posZ-theDetector->tank_SizeZ; 
  zmax = theDetector->tank_posZ+theDetector->tank_SizeZ; 

  energy_hist = new TH3D("energy_hist", "Histogram of the comulative energy loss", (xmax-xmin)/0.5, xmin, xmax, (ymax-ymin)/0.5, ymin, ymax, (zmax-zmin)/0.5, zmin, zmax); 
  LET_hist = new TH3D("LET_hist", "Histogram of the comulative LET", (xmax-xmin)/0.5, xmin, xmax, (ymax-ymin)/0.5, ymin, ymax, (zmax-zmin)/0.5, zmin, zmax); 
  DMLET_hist = new TH3D("DMLET_hist", "Histogram of the LETd", (xmax-xmin)/0.5, xmin, xmax, (ymax-ymin)/0.5, ymin, ymax, (zmax-zmin)/0.5, zmin, zmax); 
  if(theConfig->fMITSetup){
     t = new TTree("PS","PhaseSpace measured with CMOS sensors"); 
     t->Branch("x", &x,"x[6]/D"); 
     t->Branch("y", &y,"y[6]/D"); 
     t->Branch("z", &z,"z[6]/D"); 
     t->Branch("E", &Edep,"Edep[6]/D"); 
     t->Branch("Ekin", &Ekin, "Ekin[6]/D"); 
     t->Branch("EID", &EID); 
     t->Branch("PID", &PID); 
     t->Print(); 
  }
  flu = new TTree("fluence", "PhaseSpace after collimator"); 
  flu->Branch("x", &x_flu,"x_flu/D"); 
  flu->Branch("y", &y_flu,"y_flu/D"); 
  flu->Branch("E", &Edep_flu,"Edep_flu/D"); 
  flu->Branch("Ekin", &Ekin_flu, "Ekin_flu/D"); 
  flu->Branch("EID", &EID_flu); 
  flu->Branch("PID", &PID_flu); 
   


  for(int i =0; i<6; i++){x[i]; y[i]; z[i]; Edep[i] = 0.;} 
}


void G4TSAnalysis::analyseHit(G4Step* aStep, G4String theName)
{
  //Get Position
  G4ThreeVector pos = aStep->GetPreStepPoint()->GetPosition() + G4UniformRand()*(aStep->GetPostStepPoint()->GetPosition() - aStep->GetPreStepPoint()->GetPosition()); 

 
  // Get kinetic energy
  G4Track * theTrack = aStep  ->  GetTrack();
  G4double kineticEnergy =  theTrack -> GetKineticEnergy();  
  
  G4ParticleDefinition *particleDef = theTrack -> GetDefinition();
  //Get particle name  
  //G4String particleName =  particleDef -> GetParticleName();  
  
  G4int pdg = particleDef ->GetPDGEncoding();
  
  // Get unique track_id (in an event)
  G4int trackID = theTrack -> GetTrackID();
  
  G4double energyDeposit = aStep -> GetTotalEnergyDeposit();
  
  G4double DX = aStep -> GetStepLength();
  G4int Z = particleDef-> GetAtomicNumber();
  G4int A = particleDef-> GetAtomicMass();

  // From the Geant4 HadronTherapy Example
  if ( !(Z==0 && A==1) ) // All but not neutrons 
  {
    if( energyDeposit>0. && DX >0. )
      {
        if (pdg !=22) // not gamma
        {
          energy_hist->Fill(pos[0],pos[1],pos[2],energyDeposit); 
          LET_hist->Fill(pos[0],pos[1],pos[2], energyDeposit*(energyDeposit/DX)); 
        }
        else if (kineticEnergy > 50.*keV) // gamma cut
        {  
	  energy_hist->Fill(pos[0],pos[1],pos[2],energyDeposit); 
          LET_hist->Fill(pos[0],pos[1],pos[2], energyDeposit*(energyDeposit/DX)); 
        }
  
      }
  }


}

void G4TSAnalysis::analyseFluence(G4Step *aStep){
     if(aStep->GetPostStepPoint()->GetStepStatus() == fGeomBoundary){
        PID_flu = aStep->GetTrack()->GetDefinition()->GetPDGEncoding();
        x_flu = aStep->GetPostStepPoint()->GetPosition()[0];
        y_flu = aStep->GetPostStepPoint()->GetPosition()[1];
        z_flu = aStep->GetPostStepPoint()->GetPosition()[2];
        Ekin_flu = aStep->GetTrack()->GetKineticEnergy();
        EID_flu = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
        flu->Fill(); 
    }

}

void G4TSAnalysis::analyseCMOSHits(G4Step *aStep, G4int copyNumber){
   if(aStep->GetPostStepPoint()->GetStepStatus() == fGeomBoundary){
        PID = aStep->GetTrack()->GetDefinition()->GetPDGEncoding();
        x[copyNumber] = aStep->GetPostStepPoint()->GetPosition()[0]; 
        y[copyNumber] = aStep->GetPostStepPoint()->GetPosition()[1]; 
        z[copyNumber] = aStep->GetPostStepPoint()->GetPosition()[2];
        Ekin[copyNumber] = aStep->GetTrack()->GetKineticEnergy(); 
        EID = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID(); 
        /*if(aStep->GetStepLength()>0){
            Edep[copyNumber] = aStep->GetTotalEnergyDeposit()/aStep->GetStepLength(); // LET which will be times 14 mum 
        }*/ 
    }
    Edep[copyNumber] += aStep->GetTotalEnergyDeposit(); 
    
}

void G4TSAnalysis::saveCMOSHits(){
    t->Fill();
    for (int i =0; i<6; i++){
        if(theConfig->fDebug) cout << x[i] <<" " << y[i] << " "<< z[i] <<" "<< Edep[i] << " "<< PID <<"\n";
        x[i] = 0; y[i] = 0; z[i] = 0; Edep[i] = 0; 
    }
    PID = -999; 
    EID = -999; 
}

void G4TSAnalysis::Save(){
  DMLET_hist = LET_hist; 
  DMLET_hist->Divide(energy_hist);
  f1->cd();
  energy_hist->Write("energy_hist"); 
  LET_hist->Write("LET_hist"); 
  DMLET_hist->Write("DMLET_hist"); 
  if(theConfig->fMITSetup) t->Write();  
  flu->Write(); 
  f1->Close();
}

