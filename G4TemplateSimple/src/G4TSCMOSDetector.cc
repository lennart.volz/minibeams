#include "G4TSCMOSDetector.hh"
#include "G4TSAnalysis.hh"

G4TSCMOSDetector::G4TSCMOSDetector(G4String name):G4VSensitiveDetector(name),theName(name)
{
  theAnalysis = G4TSAnalysis::GetInstance();
}

G4bool G4TSCMOSDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{ 
  theAnalysis->analyseCMOSHits(aStep, fast_atoi(theName));

  return true;
}

int G4TSCMOSDetector::fast_atoi(const char* str){ // I know there is only ints <10
    int val = *str++ - '0';
    return val;
}
