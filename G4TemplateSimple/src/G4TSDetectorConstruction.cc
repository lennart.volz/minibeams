//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "G4TSDetectorConstruction.hh"
#include <iostream>
#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4Color.hh"
#include "CADMesh.hh"
#include "G4VisExtent.hh"
#include "G4VisAttributes.hh"
#include "Config.hh"
#include "G4TSSensitiveDetector.hh"
#include "G4TSCMOSDetector.hh"
#include "G4TSFluenceScorer.hh"

#define PI 3.14159265

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4TSDetectorConstruction* G4TSDetectorConstruction::theDetector=NULL;

G4TSDetectorConstruction::G4TSDetectorConstruction()
: G4VUserDetectorConstruction(),fScoringVolume(0)
{ 
  theDetector = this;
  theConfig = Config::GetInstance(); 
  theName = theConfig->fCollimatorMesh;

  //Precompute all constants needed to define world
  if(theConfig->fCollimator){
      // First we create the CADmesh solid
      // The CADmesh will define the world size 
      auto mesh = CADMesh::TessellatedMesh::FromSTL(theName.data());
    
      mesh_solid = mesh->GetSolid();
      mesh_Xmin = mesh_solid->GetExtent().GetXmin(); 
      mesh_Xmax = mesh_solid->GetExtent().GetXmax();
      mesh_Ymin = mesh_solid->GetExtent().GetYmin();
      mesh_Ymax = mesh_solid->GetExtent().GetYmax();
      mesh_Zmin = mesh_solid->GetExtent().GetZmin();
      mesh_Zmax = mesh_solid->GetExtent().GetZmax(); 
      mesh_SizeX = mesh_Xmax - mesh_Xmin; 
      mesh_SizeY = mesh_Ymax - mesh_Ymin; 
      mesh_SizeZ = mesh_Zmax - mesh_Zmin; 
  }else{
	mesh_SizeX = mesh_SizeY = mesh_SizeZ = 0.; 	
  }

  if(theConfig->fPE){
        // First we create the CADmesh solid
        // The CADmesh will define the world size 
        auto PEmesh = CADMesh::TessellatedMesh::FromSTL(theConfig->fPEMesh.data());
        PEmesh_solid = PEmesh->GetSolid();
        PEmesh_Xmin = PEmesh_solid->GetExtent().GetXmin();
        PEmesh_Xmax = PEmesh_solid->GetExtent().GetXmax();
        PEmesh_Ymin = PEmesh_solid->GetExtent().GetYmin();
        PEmesh_Ymax = PEmesh_solid->GetExtent().GetYmax();
        PEmesh_Zmin = PEmesh_solid->GetExtent().GetZmin();
        PEmesh_Zmax = PEmesh_solid->GetExtent().GetZmax();
	PE_SizeX = PEmesh_Xmax - PEmesh_Xmin; 
	PE_SizeY = PEmesh_Ymax - PEmesh_Ymin; 
	PE_SizeZ = PEmesh_Zmax - PEmesh_Zmin;
   }else{
	PE_SizeY = PE_SizeX = PE_SizeZ = 0.; 
  }
  
  if (theConfig->fModulator){
	auto Modmesh = CADMesh::TessellatedMesh::FromSTL(theConfig->fModulatorMesh.data());
        Modmesh_solid = Modmesh->GetSolid();
        Modmesh_Xmin = Modmesh_solid->GetExtent().GetXmin();
        Modmesh_Xmax = Modmesh_solid->GetExtent().GetXmax();
        Modmesh_Ymin = Modmesh_solid->GetExtent().GetYmin();
        Modmesh_Ymax = Modmesh_solid->GetExtent().GetYmax();
        Modmesh_Zmin = Modmesh_solid->GetExtent().GetZmin();
        Modmesh_Zmax = Modmesh_solid->GetExtent().GetZmax();
        Mod_SizeX = Modmesh_Xmax - Modmesh_Xmin;
        Mod_SizeY = Modmesh_Ymax - Modmesh_Ymin;
        Mod_SizeZ = Modmesh_Zmax - Modmesh_Zmin;
  	
  }else{
	Mod_SizeY = Mod_SizeX = Mod_SizeZ = 0.; 
  }
  //Lateral World dimensions only depend on mesh
  (mesh_SizeX > Mod_SizeX) ? world_sizeX = mesh_SizeX*mm : world_sizeX = Mod_SizeX*mm; //10*cm;
  (mesh_SizeY > Mod_SizeY) ? world_sizeY = mesh_SizeY*mm : world_sizeY = Mod_SizeY*mm; //10*cm;

  //Account for mispositioning of collimator in world volume size
  if(abs(theConfig->fInclination)>0 ){ 
      world_sizeX = 75*mm; //world_sizeX*mm*cos(theConfig->fInclination*PI/180.) + mesh_SizeZ*mm*sin(theConfig->fInclination*PI/180.);
      G4cout << mesh_SizeX << " " << mesh_SizeZ << " " << world_sizeX << G4endl;  
  }
  world_sizeX+=theConfig->fShiftX*mm;
 
  //Fluence scorer thickness to score secondary spectrum right after MBC
  fluscorer_thickness = 0.01*mm; 
 
  // size of the water tank
  tank_SizeX = 0.5*world_sizeX*mm;
  tank_SizeY = 0.5*world_sizeY*mm;
  tank_SizeZ = 0.5*20*cm; 

  // World Z dimension function of mesh and water tank
  world_sizeZ  = mesh_SizeZ*mm + PE_SizeZ*mm + Mod_SizeZ*mm + theConfig->fAirGap*mm + 2*tank_SizeZ + theConfig->fModCollGap*mm + 10*mm; //10 mm tolerance
 

  // Positioning of components
  modulator_pos = -0.5*world_sizeZ; // Modulator always first   
  collimator_pos = modulator_pos + Mod_SizeZ*mm + theConfig->fModCollGap*mm; // collimator second
  PE_pos = collimator_pos + mesh_SizeZ*mm*cos(theConfig->fInclination*PI/180.) + mesh_SizeX*mm*sin(theConfig->fInclination*PI/180.); //then neutron shield // TODO needs improvement!
  G4cout << collimator_pos << " " << PE_pos << G4endl; 
  tank_posZ = PE_pos + PE_SizeZ*mm + theConfig->fAirGap*mm + tank_SizeZ; // water tank origin is in its center, air gap between collimator and tank is considered 

  if(theConfig->fMITSetup){ 
      collimator_pos = 0.;
      PE_pos = collimator_pos + mesh_SizeZ*mm*cos(theConfig->fInclination*PI/180.) + mesh_SizeX*mm*sin(theConfig->fInclination*PI/180.);  
      world_sizeZ = 2*(101.9*cm + nozzle_thickness*mm + 2*mm); //2 mm epsilon
  } 
 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4TSDetectorConstruction::~G4TSDetectorConstruction()
{theDetector=NULL;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* G4TSDetectorConstruction::Construct()
{ 

  G4cout << "<<< Starting to construct the geometry." << G4endl; 	

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  // Material
  //
      
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  // DEFAULT DEFINITION OF THE MATERIALS
  // All elements and compound definition follows the NIST database
  
  // ELEMENTS
  G4bool isotopes = false;
  //G4Material* aluminumNist = G4NistManager::Instance()->FindOrBuildMaterial("G4_Al", isotopes);
  //G4Material* tantalumNist = G4NistManager::Instance()->FindOrBuildMaterial("G4_Ta", isotopes); 
  //G4Material* copperNistAsMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Cu", isotopes);
  G4Element* zincNist = nist->FindOrBuildElement("Zn");
  G4Element* copperNist = nist->FindOrBuildElement("Cu");
  G4Element* elH = nist->FindOrBuildElement("H"); 
  G4Element* elO = nist->FindOrBuildElement("O"); 
  G4Element* elSi = nist->FindOrBuildElement("Si"); 
  
  // COMPOUND
  G4Material* airNist = nist->FindOrBuildMaterial("G4_AIR", isotopes);
  G4Material* waterNist = nist->FindOrBuildMaterial("G4_WATER");
  G4Material* PE = nist->FindOrBuildMaterial("G4_POLYETHYLENE"); 

  //G4Material* kaptonNist = G4NistManager::Instance()->FindOrBuildMaterial("G4_KAPTON", isotopes);
  //G4Material* galacticNist = G4NistManager::Instance()->FindOrBuildMaterial("G4_Galactic", isotopes);
  G4Material* PMMA = nist->FindOrBuildMaterial("G4_PLEXIGLASS", isotopes);
  //G4Material* mylarNist = G4NistManager::Instance()->FindOrBuildMaterial("G4_MYLAR", isotopes);
  G4Material* siliconNist = nist->FindOrBuildMaterial("G4_Si");

  G4double d; // Density
  G4int nComponents;// Number of components
  G4int natoms; 
  G4double fractionmass; // Fraction in mass of an element in a material

  //quartz (SiO2, crystalline)
  d = 2.64 *g/cm3;
  G4Material* quartz = new G4Material("Quartz", d, nComponents= 2);
  quartz-> AddElement(elSi, natoms=1);
  quartz-> AddElement(elO,  natoms=2);
 
  // BC400 
  d = 1.032 *g/cm3; 
  G4Material *BC400 = new G4Material("BC400", d, nComponents=2); 
  BC400->AddElement(elH, fractionmass = 8.5*perCent);
  BC400->AddElement(elO, fractionmass = 91.5*perCent); 
  
  d = 8.40*g/cm3;
  nComponents = 2;
  G4Material* brass = new G4Material("Brass", d, nComponents);  
  brass -> AddElement(zincNist, fractionmass = 30 *perCent);
  brass -> AddElement(copperNist, fractionmass = 70 *perCent);
 
  G4Material *StainlessSteel = new G4Material("StainlessSteel",   8.02*g/cm3, 5);
  StainlessSteel->AddElement(nist->FindOrBuildElement("Mn"), 0.02);
  StainlessSteel->AddMaterial(nist->FindOrBuildMaterial("G4_Si"), 0.01);
  StainlessSteel->AddElement(nist->FindOrBuildElement("Cr"), 0.19);
  StainlessSteel->AddElement(nist->FindOrBuildElement("Ni"), 0.10);
  StainlessSteel->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"), 0.68);

  G4Material *lowdens_water = new G4Material("lowdens_water", 0.0054*g/cm3, 2);
  lowdens_water->AddElement(elH, 2); //0.111894);
  lowdens_water->AddElement(elO, 1); //0.888106);


  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  // Geometry

  G4bool checkOverlaps = false;


  //     
  // World
  //
  G4Box* solidWorld =    
    new G4Box("World",                       //its name
       0.5*world_sizeX, 0.5*world_sizeY, 0.5*world_sizeZ);     //its size
      
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        airNist,           //its material
                        "World");            //its name
                                   
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking
  


  //Collimator mesh placement
  if(theConfig->fUseSTL){
      if(theConfig->fCollimator){
    	auto mesh_logical = new G4LogicalVolume( mesh_solid
    	                                         , brass //StainlessSteel //brass
    	                                         , "logical"
    	                                         , 0, 0, 0
    	);
    	G4RotationMatrix *rot = new G4RotationMatrix; 
    	rot->rotateY(theConfig->fInclination*PI/180.); 
    
    	new G4PVPlacement( rot
    	                       , G4ThreeVector(0.,0.,collimator_pos)
    	                       , mesh_logical
    	                       , "physical"
    	                       , logicWorld
    	                       , false, 0
    	      );
    	G4VisAttributes *mesh_att = new G4VisAttributes(G4Color::Yellow());
    	mesh_att->SetForceSolid(true);
    	mesh_att->SetVisibility(true);
    	mesh_logical->SetVisAttributes(mesh_att);
      }
      if(theConfig->fPE){
    	if(PEmesh_Xmin!=mesh_Xmin || PEmesh_Xmax!=mesh_Xmax || 
    			PEmesh_Ymin!=mesh_Ymin || PEmesh_Ymax!=mesh_Ymax){
    		G4cout << "<WARNING:> PE mesh lateral dimensions and collimator mesh lateral dimensions are not equal. Make sure to check the geometry with -v." << G4endl; 
    	}
    	auto PEmesh_logical = new G4LogicalVolume(PEmesh_solid
                                               , PE
                                               , "logical"
                                               , 0, 0, 0
      	);
    
      	new G4PVPlacement( 0
                             , G4ThreeVector(0.,0.,PE_pos)
                             , PEmesh_logical
                             , "PEphysical"
                             , logicWorld
                             , false, 0
            );
      	G4VisAttributes *PEmesh_att = new G4VisAttributes(G4Color::White());
      	PEmesh_att->SetForceSolid(true);
      	PEmesh_att->SetVisibility(true);
    	PEmesh_logical->SetVisAttributes(PEmesh_att);
    
      }
      //scorer box for particle flux and energy spectra after Collimator 
      G4Box *fluence_scorer_solid = new G4Box("fluence_scorer_solid", world_sizeX*0.5, world_sizeY*0.5, fluscorer_thickness*0.5); 
      G4LogicalVolume *fluence_scorer_logic = new G4LogicalVolume(fluence_scorer_solid, airNist, "fluence_scorer_logic"); 
      new G4PVPlacement(0, G4ThreeVector(0,0,PE_pos + PE_SizeZ*0.5*mm + fluscorer_thickness*0.5), fluence_scorer_logic,"fluence_scorer_phys", logicWorld,false,0); // PE Mesh not useable at the moment with rotation 
      G4TSFluenceScorer *fluence_scorer = new G4TSFluenceScorer("fluence"); 
      fluence_scorer_logic->SetSensitiveDetector(fluence_scorer);
       
      if(theConfig->fModulator){
    	auto Modmesh_logical = new G4LogicalVolume(Modmesh_solid
                                               , PMMA
                                               , "logical"
                                               , 0, 0, 0
      	);
    
      	new G4PVPlacement( 0
                             , G4ThreeVector(-50.*mm,-50.*mm,modulator_pos) // Origin of modulator shifted wrt center
                             , Modmesh_logical
                             , "Modphysical"
                             , logicWorld
                             , false, 0
            );
      	G4VisAttributes *Modmesh_att = new G4VisAttributes(G4Color::White());
      	Modmesh_att->SetForceSolid(true);
      	Modmesh_att->SetVisibility(true);
    	Modmesh_logical->SetVisAttributes(Modmesh_att);
    
      }
  }

  if(!theConfig->fMITSetup){
      //
      // Water Tank
      //
      //For now its lateral size is equal to the world size
      G4Box * solidWaterTank	= new G4Box("WaterTank", tank_SizeX, tank_SizeY, tank_SizeZ);
      G4LogicalVolume *logicWaterTank = new G4LogicalVolume(solidWaterTank, waterNist, "logicWaterTank");
      new G4PVPlacement(0, G4ThreeVector(0,0, tank_posZ+fluscorer_thickness), logicWaterTank,"physWaterTank",logicWorld,false,0);
      G4VisAttributes *water_att = new G4VisAttributes(G4Color::Blue());
      water_att->SetVisibility(true);
      water_att->SetForceSolid(true); 
      logicWaterTank->SetVisAttributes(water_att);
      if(theConfig->fLET){
          G4TSSensitiveDetector *water_scorer = new G4TSSensitiveDetector("water_scorer"); 
          logicWaterTank->SetSensitiveDetector(water_scorer);  
      }
      // make this a region of specific steps 
      //TODO       


  }else{
      //Nozzle
      G4Box* solidNozzle = new G4Box("solidNozzle", world_sizeX*0.5, world_sizeY*0.5, nozzle_thickness*mm*0.5); 
      G4LogicalVolume* logicNozzle = new G4LogicalVolume(solidNozzle, lowdens_water, "logicNozzle"); 
      G4VisAttributes *nozzle_vis = new G4VisAttributes(G4Color::Blue()); 
      nozzle_vis->SetForceSolid(true); 
      nozzle_vis->SetVisibility(true); 
      logicNozzle->SetVisAttributes(nozzle_vis); 
      new G4PVPlacement(0, G4ThreeVector(0,0,-101.9*cm - nozzle_thickness*0.5), logicNozzle, "physNozzle", logicWorld, false, 0);        

      //Scintillator
      G4Box *solidScintillator = new G4Box("solidScintillator", world_sizeX*0.5, world_sizeY*0.5, 5*mm*0.5); 
      G4LogicalVolume *logicScintillator = new G4LogicalVolume(solidScintillator, BC400, "logicScintillator"); 
      new G4PVPlacement(0, G4ThreeVector(0,0,-62.9*cm), logicScintillator, "physScintillator", logicWorld, false,0); 
      
      //MIMOSA Detectors
      G4Box* solidNonSensitive1 = new G4Box("NonSensitive1", 0.5*19.9*mm, 0.5*19.2*mm, 0.5*0.006*mm);
      G4Box* solidNonSensitive2 = new G4Box("NonSensitive2", 0.5*19.9*mm, 0.5*19.2*mm, 0.5*0.030*mm);
      G4Box* solidEpitax = new G4Box("Sensor", 0.5*19.9*mm, 0.5*19.2*mm, 0.5*0.014*mm);
      G4LogicalVolume* LogicSensor[6]; 
      G4LogicalVolume* LogicNonSensitive1 = new G4LogicalVolume(solidNonSensitive1, quartz, "logicInsensitive1");
      G4LogicalVolume* LogicNonSensitive2 = new G4LogicalVolume(solidNonSensitive2, siliconNist, "logicInsensitive2");


      G4TSCMOSDetector* mimosa_sens[6]; 
      G4double sensor_pos_z[6] = {-12.9*cm, -10.7*cm, -8.5*cm, 10.3*cm, 12.5*cm, 14.7*cm};
      G4VisAttributes *sensor_att = new G4VisAttributes(G4Color::Yellow());
      sensor_att->SetVisibility(true);           
      sensor_att->SetForceSolid(true); 
     
      for(int i=0; i<6; i++){
            LogicSensor[i] =  new G4LogicalVolume(solidEpitax, siliconNist, "logicSensor"+std::to_string(i));
            new G4PVPlacement(0, G4ThreeVector(0,0, sensor_pos_z[i]+0.5*0.006*mm), LogicNonSensitive1,"physNonSensitive"+std::to_string(i),logicWorld,false,i);
            new G4PVPlacement(0, G4ThreeVector(0,0, sensor_pos_z[i]+0.006*mm+0.5*0.014*mm), LogicSensor[i],"physSensitiveSensor"+std::to_string(i),logicWorld,false,i);
            new G4PVPlacement(0, G4ThreeVector(0,0, sensor_pos_z[i]+0.006*mm+0.014*mm+0.5*0.030*mm),LogicNonSensitive2,"physNonSensitive2"+std::to_string(i),logicWorld,false,i);
            //mimosa_sens[i] = new G4TSCMOSDetector(std::to_string(i));
            //LogicSensor[i]->SetSensitiveDetector(mimosa_sens[i]); 
            LogicSensor[i]->SetVisAttributes(sensor_att);
      }
      
      //Killer Box 
      G4Box * solidKillerTank    = new G4Box("WaterTank", world_sizeX*0.5, world_sizeY*0.5, 0.01*mm*0.5);
      G4LogicalVolume *logicKillerTank = new G4LogicalVolume(solidKillerTank, airNist, "logicWaterTank");
      new G4PVPlacement(0, G4ThreeVector(0,0, sensor_pos_z[5] + 1*cm), logicKillerTank,"physWaterTank",logicWorld,false,0);
      G4VisAttributes *killer_att = new G4VisAttributes(G4Color::Blue());
      killer_att->SetVisibility(true);
      logicKillerTank->SetVisAttributes(killer_att);
      
      G4TSSensitiveDetector *killer = new G4TSSensitiveDetector("killer");
      logicKillerTank->SetSensitiveDetector(killer);

  }
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
