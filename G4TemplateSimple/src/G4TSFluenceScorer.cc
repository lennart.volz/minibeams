#include "G4TSFluenceScorer.hh"
#include "G4TSAnalysis.hh"

G4TSFluenceScorer::G4TSFluenceScorer(G4String name):G4VSensitiveDetector(name),theName(name)
{
  theAnalysis = G4TSAnalysis::GetInstance();
}

G4bool G4TSFluenceScorer::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  theAnalysis->analyseFluence(aStep);

  return true;
}
