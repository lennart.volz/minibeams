//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//

#include "G4TSPrimaryGeneratorAction.hh"

#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "G4NucleiProperties.hh"
#include "G4PhysicalConstants.hh"
#include "G4TSDetectorConstruction.hh"
#include "Config.hh"
#include <numeric>

std::vector<std::string> split(std::string, std::string); 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4TSPrimaryGeneratorAction::G4TSPrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),
   fParticleGun(0)
{
  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);
  theConfig = Config::GetInstance(); 

  userA = theConfig->fA; 
  userZ = theConfig->fZ; 

  div = theConfig->fDivergence; 
  div_x = div; 
  div_y = div; 

  if(theConfig->fFieldSizeX>0 && theConfig->fFieldSizeY>0){
    xmax = theConfig->fFieldSizeX*mm;
    xmin = -1*xmax;
    ymax = theConfig->fFieldSizeY*mm; 
    ymin = -1*ymax;   

  }else if(G4TSDetectorConstruction::GetInstance()->mesh_Xmax > 0){
    xmin = G4TSDetectorConstruction::GetInstance()->mesh_Xmin*mm + 15*mm;
    xmax = G4TSDetectorConstruction::GetInstance()->mesh_Xmax*mm - 15*mm;
    ymin = G4TSDetectorConstruction::GetInstance()->mesh_Ymin*mm + 15*mm;
    ymax = G4TSDetectorConstruction::GetInstance()->mesh_Ymax*mm - 15*mm;
  }else if(G4TSDetectorConstruction::GetInstance()->Modmesh_Xmax > 0){
    xmin = G4TSDetectorConstruction::GetInstance()->Modmesh_Xmin*mm - 50*mm;
    xmax = G4TSDetectorConstruction::GetInstance()->Modmesh_Xmax*mm - 50*mm;
    ymin = G4TSDetectorConstruction::GetInstance()->Modmesh_Ymin*mm - 50*mm;
    ymax = G4TSDetectorConstruction::GetInstance()->Modmesh_Ymax*mm - 50*mm;
  }
  else{
    xmin = xmax = ymin = ymax = 0.; 
  }
 
  angle_theta = 0.;
  angle_phi = 0.;
  G4cout << "Mesh start x: " <<xmin << "; Mesh end X:" << xmax << endl; 
  if(theConfig->fGaus && !theConfig->fMITSetup && !theConfig->fFromRST){

    //Construct the irradiation raster points
    x_center = linspace(xmin, xmax, theConfig->fSpotStep*mm);  
    y_center = linspace(ymin, ymax, theConfig->fSpotStep*mm);
    nSpotsX = x_center.size(); nSpotsY = y_center.size(); 

    // center the radiation field on the Mesh
    transform(x_center.begin(), x_center.end(), x_center.begin(),
          bind2nd(std::plus<double>(), 0.5*(xmax - x_center.back())+theConfig->fShiftX*mm));
    transform(y_center.begin(), y_center.end(), y_center.begin(),
          bind2nd(std::plus<double>(), 0.5*(ymax - y_center.back())));

    for(auto it = std::begin(x_center); it!=std::end(x_center); ++it) G4cout << *it << G4endl;
    for(auto it = std::begin(y_center); it!=std::end(y_center); ++it) G4cout << *it << G4endl;
    G4cout << "There are " << x_center.size() << " spots in x direction\n"; 
    G4cout << "There are " << y_center.size() << " spots in y direction\n";  
  }
  else if(theConfig->fFromRST){
    G4cout << "Beginning parsing of rst file "+theConfig->fRST << G4endl; 
    ParseRST(theConfig->fRST); 
    G4cout << "Finished parsing of rst file. There are "+std::to_string(nSpots) << " spots in the plan." << G4endl;  
    G4float planpercent = float(theConfig->fN)/float(accumulate(nParticles.begin(), nParticles.end(), 0));
    G4cout << "<NOTE> We will simulate "+std::to_string(planpercent)+"% of the total planned particles." << G4endl; 
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4TSPrimaryGeneratorAction::~G4TSPrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void G4TSPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  particle
    = G4IonTable::GetIonTable()->GetIon(userZ,userA,0);
  fParticleGun->SetParticleDefinition(particle);

  ionE = theConfig->fEnergy*(G4NucleiProperties::GetNuclearMass(userA, userZ)/amu_c2);
 

  // Only change if you know what you do after here
  // Get size of world
  G4double worldZHalfLength = 0;
  G4LogicalVolume* worlLV
    = G4LogicalVolumeStore::GetInstance()->GetVolume("World");
  G4Box* worldBox = 0;
  if ( worlLV) worldBox = dynamic_cast< G4Box*>(worlLV->GetSolid()); 
  if ( worldBox ) {
    worldZHalfLength = worldBox->GetZHalfLength();  
  }
  else  {
    G4ExceptionDescription msg;
    msg << "World volume of box not found." << G4endl;
    msg << "Perhaps you have changed geometry." << G4endl;
    msg << "The gun will be place in the center.";
    G4Exception("B4PrimaryGeneratorAction::GeneratePrimaries()",
      "MyCode0002", JustWarning, msg);
  } 
  
   
 
  //area of particle generation
  //centered beam
  //G4double X = 0.;
  //G4double Y = 0.;
  //gaussian beam spot
  //G4double X = G4RandGauss::shoot(0,0.2);
  //G4double Y = G4RandGauss::shoot(0,0.2);
  //rect field 

  if(!theConfig->fGaus && !theConfig->fMITSetup && !theConfig->fFromRST){
  	X = G4RandFlat::shoot(xmin,xmax);
  	Y = G4RandFlat::shoot(ymin,ymax);  
  }else if (theConfig->fMITSetup){
        G4double mean_x = 3.96728*mm; 
        G4double std_x = 3.69055*mm;  
        if(G4UniformRand()>=0.4540042){ 
           mean_x = -2.23455*mm; 
           std_x = 2.83125*mm; 
        }
        X =G4RandGauss::shoot(mean_x, std_x);
	Y =G4RandGauss::shoot(-0.0357233*mm, 4.7*mm); //5.41416*mm);
  }else if(theConfig->fFromRST){
        int current = int(mDistriGeneral->fire()*nSpots);
        X = G4RandGauss::shoot(x_center[current], foci[current]*mm/2.355);// division by 2.355 to go from FWHM to sigma
        Y = G4RandGauss::shoot(y_center[current], foci[current]*mm/2.355);  
        if(theConfig->fDebug){
	   G4cout << "Spot is " << X << " " << Y << endl;
	}
        ionE = energies[current]*userA;
  }else{
	spot_x = int(G4UniformRand()*nSpotsX); //To fill all spots uniformly 
	spot_y = int(G4UniformRand()*nSpotsY);
	X = G4RandGauss::shoot(x_center[spot_x], theConfig->fSpotSize*mm);
	Y = G4RandGauss::shoot(y_center[spot_y], theConfig->fSpotSize*mm);
	if(theConfig->fDebug){
	   G4cout << "Spot is " << X << " " << Y << endl;
	}

  }
  fParticleGun->SetParticlePosition(G4ThreeVector(X, Y, -worldZHalfLength));

  //particle energy
  //mono energetic
  //fParticleGun->SetParticleEnergy(ionE*MeV);
  //gaus energy spread
  fParticleGun->SetParticleEnergy(G4RandGauss::shoot(ionE*MeV,ionE/1000.)*MeV); // 0.1% gaussian energy spread
 
  //beam divergence
  //non
  if(theConfig->fSource){ angle_theta = asin(X/theConfig->fSource); angle_phi = asin(Y/theConfig->fSource);}
  if(theConfig->fMITSetup){angle_theta = 0.; angle_phi = 0.; div_x=1.8*mrad; div_y=1.8*mrad;} // Directly before the nozzle exit window //4*mrad and 3* mrad in y, div_y claire = 1.7mrad
  if(!(div_x && div_y)){
  	fParticleGun->SetParticleMomentumDirection(G4ThreeVector(angle_theta,angle_phi,1.));//no div
  //divergence using gauss distribution
  }else{
  	fParticleGun->SetParticleMomentumDirection(G4ThreeVector(G4RandGauss::shoot(angle_theta*mrad,div*mrad), G4RandGauss::shoot(angle_phi*mrad,div*mrad),1.));
  }
  //divergence nils basler //not super tested
  /*
  fdX0  = 2.8; 
  fdY0  = 3.5;  
  //focusing
  fkX = -500; fkY = -1300;
  fwX  = 2.5; fwY  = 3.5;
  
  X = (G4RandGauss::shoot(0.,fwX/2.3548));
  Y = (G4RandGauss::shoot(0.,fwY/2.3548));
  
  //gauss beam spot
  //X0 = (G4RandGauss::shoot(0.,fdX0/2.3548)+fkX*X/1000) ;
  //Y0 = (G4RandGauss::shoot(0.,fdY0/2.3548)+fkY*Y/1000) ;
  //fParticleGun->SetParticlePosition(G4ThreeVector(X0,Y0,Z0));
  
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(asin(X/1000),asin(Y/1000),1.));
  */
 
  fParticleGun->GeneratePrimaryVertex(anEvent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
std::vector<double> G4TSPrimaryGeneratorAction::linspace(double start, double stop, double step){
	std::vector<double> result; 
	for(double x = start; x<=stop; x+=step){
		result.push_back(x);
	}
	return result; 
}

void G4TSPrimaryGeneratorAction::ParseRST(G4String RstFile){
    std::ifstream file(RstFile.data());
    std::string str; 
    G4double energy,focus;  
    int state =0; // 0 state is preamble or switch submachine, state 1 is after #points
    std::vector<std::string> helper; 
    nSpots=0; 
    while (std::getline(file, str))
    {
        // Process str
        if(str.find("charge")!=string::npos){
           userZ = atoi(split(str," ")[1].c_str());
           cout << "Charge " << userZ << "\n";
        }
        else if(str.find("mass")!=string::npos){
           userA = atoi(split(str," ")[1].c_str());
           cout << "Mass " << userA << "\n";
        }
        else if(str.find("submachine#")!=string::npos){
            helper = split(str," ");
            energy = atof(helper[2].c_str()); 
            focus = atof(helper[4].c_str());
            state = 0; 
            cout << "Energy " << energy << "MeV/u. Focus " << focus << "mm FWHM\n"; 
        }
        else if(str.find("#points")!=string::npos){ 
            state =1; 
            continue;
        }
        else if(state==1){
            helper = split(str," ");
            x_center.push_back(atof(helper[0].c_str())); 
            y_center.push_back(atof(helper[1].c_str()));
            nParticles.push_back(int(atof(helper[2].c_str()))); 
            cout << "X " << x_center.back() << " Y " << y_center.back() << " particles " << nParticles.back() << "\n";
            energies.push_back(energy);    
            foci.push_back(focus); 
            ++nSpots; 
        }
    }
    cout << "Finished parsing of rst file" << endl; 

    // Some things partaining the reduction of number of particles
    mPDF = new double[nSpots];
    for(int i=0; i<nSpots; ++i){
         mPDF[i] = nParticles.at(i);
    }
    mDistriGeneral = new CLHEP::RandGeneral(CLHEP::HepRandom::getTheEngine(), mPDF, nSpots, 0);

}

std::vector<std::string> split(std::string str, std::string delimiter){
    size_t pos = 0;
    std::string token;
    std::vector<std::string> vec; 
    while ((pos = str.find(delimiter)) != -1) {
        token = str.substr(0, pos);
        vec.push_back(token); 
        str.erase(0, pos + delimiter.length());
    }
    vec.push_back(str); 

    return vec; 
}
