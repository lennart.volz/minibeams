#include "G4TSSensitiveDetector.hh"
#include "G4TSAnalysis.hh"

G4TSSensitiveDetector::G4TSSensitiveDetector(G4String name):G4VSensitiveDetector(name),theName(name)
{
  theAnalysis = G4TSAnalysis::GetInstance();
}

G4bool G4TSSensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  if(!theName[0]=='k') theAnalysis->analyseHit(aStep, theName);
  if (aStep->GetPostStepPoint()->GetStepStatus() == fGeomBoundary){ // if boundary is hit after step, it means the particle leaves the detetcor
    aStep->GetTrack()->SetTrackStatus(fStopAndKill);
  }

  return true;
}


