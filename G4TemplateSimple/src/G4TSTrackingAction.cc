#include "G4TSAnalysis.hh"
#include "G4TSTrackingAction.hh"

G4TSTrackingAction::G4TSTrackingAction() : G4UserTrackingAction()
{
    theAnalysis = G4TSAnalysis::GetInstance(); 
}

void G4TSTrackingAction::PostUserTrackingAction(const G4Track*){if(theAnalysis->Edep[0]>0.82 || theAnalysis->Edep[1]>0.82|| theAnalysis->Edep[2]>0.82|| theAnalysis->Edep[3]>0.82|| theAnalysis->Edep[4]>0.82|| theAnalysis->Edep[5]>0.82) theAnalysis->saveCMOSHits();}
