//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "G4TSDetectorConstruction.hh"
#include "G4TSActionInitialization.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4ScoringManager.hh"

//#include "HadrontherapyPhysicsList.hh"

#include "G4UImanager.hh"
//#include "QBBC.hh"
#include "G4PhysListFactory.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "Config.hh"
#include "Randomize.hh"
#include <stdio.h>
#include <sstream>
#include <string>

#include "G4TSAnalysis.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv)
{
  //Get input from command line
  Config theConfig; 
  theConfig.ParseCommandLine(argc, argv);


  // Optionally: choose a different Random engine...
  // G4Random::setTheEngine(new CLHEP::MTwistEngine);
  
  // Construct the default run manager
  //
#ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
#else
  G4RunManager* runManager = new G4RunManager;
#endif

  // Physics list
  //G4VModularPhysicsList* thePhysics = new QBBC;
//  HadrontherapyPhysicsList *thePhysics = new HadrontherapyPhysicsList(); 
  G4PhysListFactory physListFactory; 
  const G4String theList = "QGSP_BIC_EMZ"; //recommended for medical applications
  G4VModularPhysicsList* thePhysics = physListFactory.GetReferencePhysList(theList); 
  runManager->SetUserInitialization(thePhysics);

  // Set mandatory initialization classes
  // Detector construction
  G4TSDetectorConstruction* theDetector = new G4TSDetectorConstruction(); 
  runManager->SetUserInitialization(theDetector);

  // Analysis
  std::stringstream rootfile;
  rootfile << theConfig.fOut.c_str() << "let_a" << theConfig.fA << "_z"
            << theConfig.fZ << "_e" << theConfig.fEnergy << "_g" << theConfig.fGaus << "_s" << theConfig.fSpotSize
            << "_l" << theConfig.fSpotStep << "_r" << theConfig.fDivergence << "_x" << theConfig.fShiftX << "_w"
            << theConfig.fAirGap << "_i" << theConfig.fInclination << "_b" << theConfig.fSource
            << "_n" << theConfig.fN << ".root";
   G4String name = rootfile.str(); 
   G4TSAnalysis *theAnalysis = new G4TSAnalysis(name);
 
  // User action initialization
  runManager->SetUserInitialization(new G4TSActionInitialization()); 

  // Scoring manager
  G4ScoringManager* scoringManager = G4ScoringManager::GetScoringManager();

 
  // Initialize visualization
  //
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  // Process macro or start UI session
  //
  G4UIExecutive* ui = 0;
  if ( theConfig.fVis) {
    ui = new G4UIExecutive(argc, argv);
    // interactive mode
    UImanager->ApplyCommand("/control/execute init_vis.mac");

    // Scoring Mesh
    UImanager->ApplyCommand("/score/create/boxMesh bMesh");
    UImanager->ApplyCommand("/score/mesh/translate/xyz 0. 0. "+std::to_string(theDetector->tank_posZ)+" mm");

    UImanager->ApplyCommand("/score/mesh/boxSize "+std::to_string(theDetector->tank_SizeX)+" "+std::to_string(theDetector->tank_SizeY)+" "+std::to_string(theDetector->tank_SizeZ));


    UImanager->ApplyCommand("/score/mesh/nBin "+std::to_string(int(2*theDetector->tank_SizeX/0.1))+" "+std::to_string(1)+" "+std::to_string(int(2*theDetector->tank_SizeZ/0.5)));
    UImanager->ApplyCommand("/score/quantity/doseDeposit TDose");
    UImanager->ApplyCommand("/score/quantity/doseDeposit neutDose");
    ui->SessionStart();
    delete ui;
  }
  else {     
    //G4String command = "/control/execute ";
    //G4String fileName = argv[1];
    //UImanager->ApplyCommand(command+fileName);
 
    // batch mode
    UImanager->ApplyCommand("/run/setCut 0.5 mm");
    UImanager->ApplyCommand("/run/setCutForAGivenParticle e- 10 um"); //added 
    //UImanager->ApplyCommand("/process/inactivate msc all");
    //UImanager->ApplyCommand("/process/inactivate had all");
    //UImanager->ApplyCommand("/process/eLoss/fluct false");
    //UImanager->ApplyCommand("/process/eLoss/CSDARange true");
    UImanager->ApplyCommand("/run/initialize");


    UImanager->ApplyCommand("/control/verbose 0");
    UImanager->ApplyCommand("/run/verbose 0");
    UImanager->ApplyCommand("/event/verbose 0");
    UImanager->ApplyCommand("/tracking/verbose 0");

    // Generate the output directory
    std::stringstream command; 
    command << "mkdir -p " << theConfig.fOut.data(); 
    int mkdir_success = system(command.str().c_str());  
    if(mkdir_success){ cout << "could not make the output directory. Will abort the run\n"; exit(0); }

  
    // Scoring Mesh
    if(!theConfig.fMITSetup){ // not needed if we look at claire's stuff
        UImanager->ApplyCommand("/score/create/boxMesh bMesh");
        UImanager->ApplyCommand("/score/mesh/translate/xyz 0. 0. "+std::to_string(theDetector->tank_posZ)+" mm");
        UImanager->ApplyCommand("/score/mesh/boxSize "+std::to_string((theDetector->tank_SizeX))+" "+ std::to_string(theDetector->tank_SizeY)+" "+std::to_string(theDetector->tank_SizeZ)+" mm"); // avoid edge effects (hence -10mm)
        UImanager->ApplyCommand("/score/mesh/nBin "+std::to_string(int(2*(theDetector->tank_SizeX)/0.1))+" "+ std::to_string(1)+" "+std::to_string(int(2*theDetector->tank_SizeZ/0.5)));
        UImanager->ApplyCommand("/score/quantity/doseDeposit TDose"); 
        UImanager->ApplyCommand("/score/quantity/doseDeposit neutDose");
        UImanager->ApplyCommand("/score/filter/particle nFilter neutron");
        UImanager->ApplyCommand("/score/quantity/flatSurfaceCurrent nFluence"); 
        UImanager->ApplyCommand("/score/filter/particle nFFilter neutron");  
   

  
        std::size_t found = theConfig.fCollimatorMesh.find_last_of("/");
        std::size_t found2 = theConfig.fCollimatorMesh.find_last_of(".");
        G4String CollName = theConfig.fCollimatorMesh.substr(found+1,found2); 
        found = theConfig.fPEMesh.find_last_of("/");
        found2 = theConfig.fPEMesh.find_last_of(".");
        G4String PEName = theConfig.fPEMesh.substr(found+1,found2); 
        found = theConfig.fModulatorMesh.find_last_of("/");
        found2 = theConfig.fModulatorMesh.find_last_of(".");
        G4String ModName = theConfig.fModulatorMesh.substr(found+1,found2); 

        std::stringstream dose_score_command; 
        dose_score_command << "/score/dumpQuantityToFile bMesh TDose " << theConfig.fOut.c_str() << "/totDose_a" << theConfig.fA << "_z" 
                << theConfig.fZ << "_e" << theConfig.fEnergy << "_g" << theConfig.fGaus << "_s" << theConfig.fSpotSize 
                << "_l" << theConfig.fSpotStep << "_r" << theConfig.fDivergence << "_x" << theConfig.fShiftX << "_w" 
                << theConfig.fAirGap << "_i" << theConfig.fInclination << "_b" << theConfig.fSource 
                << "_n" << theConfig.fN << "_c" << CollName.c_str() <<"_t" << PEName.c_str() << "_m" << ModName.c_str()  << ".csv"; 
 
        UImanager->ApplyCommand(dose_score_command.str());
       
        std::stringstream ndose_score_command; 
        ndose_score_command << "/score/dumpQuantityToFile bMesh neutDose " << theConfig.fOut.c_str() << "/neutronDose_a" << theConfig.fA << "_z" 
                << theConfig.fZ << "_e" << theConfig.fEnergy << "_g" << theConfig.fGaus << "_s" << theConfig.fSpotSize 
                << "_l" << theConfig.fSpotStep << "_r" << theConfig.fDivergence << "_x" << theConfig.fShiftX << "_w" 
                << theConfig.fAirGap << "_i" << theConfig.fInclination << "_b" << theConfig.fSource 
                << "_n" << theConfig.fN << "_c" << CollName.c_str() << "_t" << PEName.c_str() << "_m" << ModName.c_str() << ".csv"; 
 
        UImanager->ApplyCommand(ndose_score_command.str());
       
        std::stringstream nfluence_score_command; 
        nfluence_score_command << "/score/dumpQuantityToFile bMesh nFluence " << theConfig.fOut.c_str() << "/neutronFluence_a" << theConfig.fA << "_z" 
	    << theConfig.fZ << "_e" << theConfig.fEnergy << "_g" << theConfig.fGaus << "_s" << theConfig.fSpotSize 
	    << "_l" << theConfig.fSpotStep << "_r" << theConfig.fDivergence << "_x" << theConfig.fShiftX << "_w" 
	    << theConfig.fAirGap << "_i" << theConfig.fInclination << "_b" << theConfig.fSource 
	    << "_n" << theConfig.fN << "_c" << CollName.c_str() << "_t" << PEName.c_str() << "_m" << ModName.c_str() << ".csv"; 
 
        UImanager->ApplyCommand(nfluence_score_command.str());
      
        UImanager->ApplyCommand("/score/close");  
    }
    UImanager->ApplyCommand("/run/printProgress 1000");
    UImanager->ApplyCommand("/run/beamOn "+std::to_string(theConfig.fN));


 }
  theAnalysis->Save(); 

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted 
  // in the main() program !
  
  delete visManager;
  delete runManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
