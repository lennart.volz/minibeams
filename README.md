This is a simple Geant4 simulation for the Minibeams project to investigate the effect of different collimator designs on the dose shape.

The code was adapted from a template simulation developed by C. Schuy. 

It comprises the following: 
templateSimple.cc	main
G4TSPrimaryGeneratoraction	Steering file for particle source
G4TSDetectorConstruction	Set up the simulation geometry, read in the Collimator stl file
G4TSActionInitialization	Initialize user action
G4TSTrackingAction              Action after each track is simulated for correct simulation of single-event CMOS data
G4TSAnalysis                    Useranalyisis class for phase space (CMOS) scoring, and LETd scoring. 
Config                          Command line parser
CADMesh                         CAD mesh reader. 

To compile it, go to G4TemplateSimple-build and run cmake ../G4TemplateSimple/ then make. 

To run it do ./templateSimple

The code takes command line arguments to steer the most important beam parameters. Running ./templatesimple -h prints help. 

Help: 

This is a simulation for minibeam collimator testing. Please use it like the following: 
Run the script: ./templateSimple -flags

-h Help; you are here 

-d Debug. Usefule console output (Not yet well implemented)

-v Visualization. Interactive mode with Geant4 QT GUI.

-n Number of particles in run(int). Default  10000. 

-e Energy in MeV per u (float). Default 180MeV/u. Sets the beam energy

-a Atomic number of projectile (int). Default 12

-z Atomic charge of projectile (int). Default 6

-g Gaussian beam (boolean). If set, a field of gaussian pencil beams is used instead of the default flat homogeneous field.

-s Spot size in mm (float). If Gaus: Spot sigma in lateral position (spot is assumed symmetric in x and y)

-l Spot spacing mm (float). If Gaus: Lateral spacing between adjacent spots.

-r Beam divergence in mrad (float). Currently the code uses a simple random gauss for momentum spread modelling. No co-variance is assumed.

-c Collimator mesh (file). Stl mesh representing the collimator, to be given with relative or absolute path and file extension. The stl file is used to set the World lateral dimensions, as well as to center the beam, and to define the rst grid in case of -g, or fildsize otherwise. The collimator material is assumed to be brass as in the beamtimes at GSI/MIT. 

-p Particle type. Instead of a and z, the code permits to use Protons, Helium ions, Carbon ions and Argon ions by name. Example -p Helium sets z = 2 and a = 4.

-o Output directory. Optional argument for output dir. Default is ".". 

-t Option to add an additional CAD mesh collimator made of PE for stopping thermal neutrons generated in the main collimator. Needs a second mesh file to work. Example usage: -t MeshFile

-x fieldsize in x. Default is the collimator width in x (or modulator width, if no collimator is specified). Give half width. The field will be [-x,x]

-y fieldsize in y. Default is the collimator height in y (or modulator height, if no collimator is specified). Give half height. The field will be [-y,y]

-m Modulator mesh (file). Similar as -t or -m option, this will read a stl file for a 3D range modulator to be placed before the collimator at distance -f. The collimator is modeled as PMMA in the code at the moment. Enables to look into 3DRM modulator doses in water and FLASH minibeams

-f Distance between collimator and modulator, or additional air gap to water tank if no collimator is used. Modulator is currently placed in front of collimator.  

-q Replication of setup at Marburg ion beam therapy center

-w Distance between collimator (or modulator, if no collimator is specified) and water tank. Default 0.

-i Inclination of collimator/modulator/neutron shield with respect to the incident beam direction. Default 0. 

-u RST file parser. Takes as argument an rst file. Atomic number, and charge, as well as energy, focus, spot positions and particle numbers will be set from the parsed RST file. Spots will be filled randomly according to their spotweight, to enable use of fewer particles than specified in plan. Default = "". 

-k Shift in x direction for misalignment test.


NOTE: It is recommended to run each change of parameters once with the -v flag for visual checking of correctness. 


The output are two csv files containing X,Y,Z,Dose,Dose^2 values. The first is total dose, the second is neutron dose. I also added a let scorer, the result of which is a root file. If -q option is chosen, the root file also contains the Phase space for the Marburg/Claire CMOS sensor setup

To visualize it, you can use PlotResult.py:

python3 PlotResult.py result.csv

To plot the difference between two result files do: 
python3 PlotResult.py result1.csv result2.csv







